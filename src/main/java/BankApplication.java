import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

public class BankApplication {
    static private int accountId;
    public static void main(String args[]) {
        Balance balance= new Balance();
        UserInfo userInfo = new UserInfo();
        Address address = new Address();
        System.out.println("********WELCOME TO YOURBANK*********\n\n");
        System.out.println("Enter 1 for savings bank account\nEnter 2 for current account");
        Scanner sc = new Scanner(System.in);
        List<UserInfo> users = new ArrayList<>();
        int choice = sc.nextInt();
        if(choice==1)
            userInfo.setType("Savings Account");
        else
            userInfo.setType("Current Account");
        System.out.println("Enter your name");
        String name = sc.next();
        userInfo.setName(name);
        System.out.println("Enter the Address in city,state,pin format\nEnter City");
        String city = sc.next();
        System.out.println("Enter state");
        String state = sc.next();
        System.out.println("Enter pin");
        String country = sc.next();
        address.setCountry(country);
        userInfo.setBalanceAmount(0.0);
        userInfo.setCreatedDate(java.time.LocalTime.now());
        userInfo.setStatus("Active");
        address.setState(state);
        address.setCity(city);
        userInfo.setAddress(address);

        users.add(userInfo);
        userInfo.setAccountId(++accountId);

        System.out.println("Enter amount to be added");
        double amount =sc.nextDouble();
        balance.addBalance(amount,userInfo);
        System.out.println("Enter amount to be withdrawn");
        double amount1 =sc.nextDouble();
        balance.withdraw(amount1, userInfo);
        System.out.println("Balance"+userInfo.getBalanceAmount());
        System.out.println("User Details"+users);





    }
}
