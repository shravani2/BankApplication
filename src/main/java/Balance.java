public class Balance {

    public void addBalance(double amount,UserInfo userInfo){
        double balance = userInfo.getBalanceAmount();
        userInfo.setBalanceAmount(balance+amount);

    }
    public  void withdraw(double amount, UserInfo userInfo){
        double balance = userInfo.getBalanceAmount();
        if(balance<amount)
            System.out.println("Not enough balance");
        else
            userInfo.setBalanceAmount(balance-amount);

    }

}
